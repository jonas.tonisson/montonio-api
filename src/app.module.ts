import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LhvModule } from './lhv/lhv.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [LhvModule, ConfigModule.forRoot()],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
