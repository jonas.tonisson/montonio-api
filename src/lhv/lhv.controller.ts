import {
  BadRequestException,
  Controller,
  Get,
  Post,
  Query,
  Req,
  Res,
} from '@nestjs/common';
import { LhvService } from './lhv.service';
import { ConfigService } from '@nestjs/config';

@Controller('lhv')
export class LhvController {
  constructor(
    private lhvService: LhvService,
    private configService: ConfigService,
  ) {}

  montonioUiUrl = this.configService.get<string>('MONTONIO_UI_URL');

  @Get('auth')
  authenticate(@Res() res) {
    return res.redirect(this.lhvService.prepareAuthRequest());
  }

  @Get('auth_callback')
  async authenticateCallback(@Res() res, @Query('code') code: string) {
    const response = await this.lhvService.requestOauthToken(code);
    res.cookie('lhv_access_token', response.data.access_token, {
      expires: new Date(Date.now() + response.data.expires_in * 1000),
    });
    res.cookie('lhv_refresh_token', response.data.refresh_token, {
      expires: new Date(Date.parse(response.data.refresh_token_expiration)),
    });
    return res.redirect(this.montonioUiUrl);
  }

  @Get('consent')
  async consent(@Req() request) {
    if (!request.cookies.lhv_access_token) {
      return 'Not authenticated';
    }
    const response = await this.lhvService.createConsent(
      request.cookies.lhv_access_token,
    );
    return response.data;
  }

  @Get('accounts')
  async accounts(@Req() request) {
    if (!request.cookies.lhv_consent_id || !request.cookies.lhv_access_token) {
      throw new BadRequestException();
    }
    const response = await this.lhvService.getUserAccounts(
      request.cookies.lhv_access_token,
      request.cookies.lhv_consent_id,
    );
    return response.data;
  }

  @Post('pay')
  async pay(@Req() request) {
    if (!request.cookies.lhv_access_token) {
      return 'Not authenticated';
    }
    const response = await this.lhvService.initiatePayment(
      request.cookies.lhv_access_token,
      request.body.account,
    );
    return response.data;
  }
}
