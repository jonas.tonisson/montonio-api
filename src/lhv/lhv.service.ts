import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom } from 'rxjs';
import { AxiosResponse } from 'axios';
import * as https from 'https';
import * as fs from 'fs';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class LhvService {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
  ) {}

  lhvUrl = this.configService.get<string>('LHV_URL');
  clientId = this.configService.get<string>('LHV_CLIENT_ID');
  montonioApiUrl = this.configService.get<string>('MONTONIO_API_URL');
  montonioUiUrl = this.configService.get<string>('MONTONIO_UI_URL');

  httpsAgent = new https.Agent({
    cert: fs.readFileSync('src/lhv/resources/lhv.crt'),
    key: fs.readFileSync('src/lhv/resources/lhv.key'),
  });

  prepareAuthRequest() {
    const url = new URL(this.lhvUrl + '/psd2/oauth/authorize');
    url.searchParams.append('scope', 'psd2');
    url.searchParams.append('response_type', 'code');
    url.searchParams.append('client_id', this.clientId);
    url.searchParams.append(
      'redirect_uri',
      this.montonioApiUrl + '/lhv/auth_callback',
    );
    url.searchParams.append('state', 'authenticated');

    return url;
  }

  requestOauthToken(code: string): Promise<AxiosResponse<any>> {
    const params = new URLSearchParams();
    params.append('client_id', this.clientId);
    params.append('grant_type', 'authorization_code');
    params.append('code', code);
    params.append('redirect_uri', this.montonioApiUrl + '/lhv/auth_callback');

    const config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      httpsAgent: this.httpsAgent,
    };

    return firstValueFrom(
      this.httpService.post(this.lhvUrl + '/psd2/oauth/token', params, config),
    );
  }

  createConsent(authToken: string): Promise<AxiosResponse<any>> {
    const currentDate = new Date();
    const data = {
      access: {
        availableAccounts: 'allAccounts',
      },
      recurringIndicator: false,
      validUntil: new Date(currentDate.setMonth(currentDate.getMonth() + 1)),
      frequencyPerDay: 100,
      combinedServiceIndicator: false,
    };

    const config = {
      headers: {
        Authorization: 'Bearer ' + authToken,
        'PSU-IP-Address': '1.2.3.4',
        'TPP-Redirect-URI': this.montonioUiUrl,
        'X-Request-ID': '99391c7e-ad88-49ec-a2ad-99ddcb1f7721',
        'Content-Type': 'application/json',
        'client-id': this.clientId,
      },
      httpsAgent: this.httpsAgent,
    };

    return firstValueFrom(
      this.httpService.post(this.lhvUrl + '/psd2/v1/consents', data, config),
    );
  }

  getUserAccounts(
    authToken: string,
    consentId: string,
  ): Promise<AxiosResponse<any>> {
    const config = {
      headers: {
        Authorization: 'Bearer ' + authToken,
        'X-Request-ID': '99391c7e-ad88-49ec-a2ad-99ddcb1f7721',
        'Consent-ID': consentId,
      },
      httpsAgent: this.httpsAgent,
    };

    return firstValueFrom(
      this.httpService.get(this.lhvUrl + '/psd2/v1/accounts', config),
    );
  }

  initiatePayment(
    authToken: string,
    debtorIban: string,
  ): Promise<AxiosResponse<any>> {
    const data = {
      debtorAccount: {
        iban: debtorIban,
      },
      instructedAmount: {
        currency: 'EUR',
        amount: '25.55',
      },
      creditorAccount: {
        iban: 'EE717700771001735865',
      },
      creditorName: 'Donald Duck',
      remittanceInformationUnstructured: 'Payment to Donald Duck',
      remittanceInformationStructured: {
        reference: 'Reference example',
      },
    };

    const config = {
      headers: {
        Authorization: 'Bearer ' + authToken,
        'PSU-IP-Address': '1.2.3.4',
        'TPP-Redirect-URI': this.montonioUiUrl,
        'TPP-Nok-Redirect-URI': this.montonioUiUrl,
        'X-Request-ID': '99391c7e-ad88-49ec-a2ad-99ddcb1f7721',
        'Content-Type': 'application/json',
        'client-id': this.clientId,
      },
      httpsAgent: this.httpsAgent,
    };

    return firstValueFrom(
      this.httpService.post(
        this.lhvUrl + '/psd2/v1.1/payments/sepa-credit-transfers',
        data,
        config,
      ),
    );
  }
}
