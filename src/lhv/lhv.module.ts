import { Module } from '@nestjs/common';
import { LhvController } from './lhv.controller';
import { LhvService } from './lhv.service';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [HttpModule, ConfigModule.forRoot()],
  controllers: [LhvController],
  providers: [LhvService],
})
export class LhvModule {}
